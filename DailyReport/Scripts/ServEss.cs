﻿using CafeNextFramework;
using CafeNextFramework.Helpers.Action;
using CafeNextFramework.TestAccess;
using OpenQA.Selenium;
using DailyReport.Pages;
using DailyReport.Utilities;
using System;
using System.IO;
using System.Threading;
using Microsoft.Office.Interop.Excel;
using System.Linq;
using CafeNextFramework.Helpers.Configuration;
using System.Data;
using System.Diagnostics;

namespace DailyReport.Scripts
{
    internal class ServEss : ITestScript
    {
        private IWebDriver driver;

        public void ExecuteScript(MasterSheetRow masterSheetRow)
        {
            Console.WriteLine("ServEss Started");
            driver = driver.Initialize(masterSheetRow);
            if (driver != null)
            {
                bool isBreachedData = Convert.ToBoolean(ConfigurationSettings.Get("BreachedReportFlag", masterSheetRow));
                bool isdailyReport = Convert.ToBoolean(ConfigurationSettings.Get("DailyReportFlag", masterSheetRow));
                bool isFutureReportFlag = Convert.ToBoolean(ConfigurationSettings.Get("FutureReportFlag", masterSheetRow));

                HomePage homePage = new HomePage(driver, masterSheetRow);

                //Perfomring Login
                homePage.PerformLogin();

                //Clicking on task
                if (homePage.ClickOnTask())
                {
                    Thread.Sleep(6000);           
                    //Switching to Frame
                    driver.SwitchTo().Frame(ServEssConstant.FRAMEID);
                    Thread.Sleep(2000);
        
                    //Selecting my open task dropdown
                    if (homePage.SelectMyOpenTask(isBreachedData, isdailyReport))
                    {
                        Thread.Sleep(4000);
                        if (!isdailyReport)
                        {
                            //Select Incident from dropdown.(ParentObject Name)
                            homePage.FilterparentObjectName(isBreachedData, isFutureReportFlag);
                        }

                        //Commenting below line, as its handled in Excel.
                        //homePage.FilterStatus();

                        //Clicking on export Button.
                        if (homePage.ClickOnExport(isdailyReport))
                        {
                            //Changing extension of the file.
                            UpdateExcelFile(masterSheetRow, isBreachedData, isdailyReport);

                            //Starting the process.
                            Process process = new Process();
                            if (isBreachedData)
                            {
                                process.StartInfo.FileName = ConfigurationSettings.Get("BreachedDataExePath", masterSheetRow);
                            }
                            else if(isdailyReport)
                            {
                                process.StartInfo.FileName = ConfigurationSettings.Get("TicketDetailsExePath", masterSheetRow);
                            }
                            else if(isFutureReportFlag)
                            {
                                process.StartInfo.FileName = ConfigurationSettings.Get("FutureBreachExePath", masterSheetRow);
                            }
                            else
                            {
                                Console.WriteLine("For execution :-> Exe path is not correct");
                            }

                            // Starting the Process
                            process.Start();

                            //Waiting for Procees to be finished
                            Thread.Sleep(5000);
                        }
                    }
                }
                //Closing the browser
                driver.CloseBrowser(masterSheetRow);
            }

        }

        //Updating excel file.
        public void UpdateExcelFile(MasterSheetRow masterSheetRow, bool breachedReportFlag, bool isDailyReport)
        {
            try
            {
                Application app = null;
                Workbook wb = null;
                string filePath = "";
                var latestFile = "";
                string path = Environment.ExpandEnvironmentVariables("%userprofile%/downloads/");
                //Below code is commented (Only applicable for Local machine)
                //path = path.Remove(0, 1);
                //path = ConfigurationSettings.Get("UserPathStartLetter", masterSheetRow) + path;
                var files = new DirectoryInfo(path).GetFiles(@"*.xlsx");
                DateTime latestUpdated = DateTime.MinValue;
                latestFile = files.Where(x => x.LastWriteTime > latestUpdated).OrderByDescending(x => x.LastWriteTime).FirstOrDefault().FullName;
                if (latestFile.Contains("~$"))
                {
                    latestFile = latestFile.Replace("~$", string.Empty);
                }
                if (breachedReportFlag)
                {
                    filePath = ConfigurationSettings.Get("BreachedFilePath", masterSheetRow);
                }
                else if (isDailyReport)
                {
                    filePath = ConfigurationSettings.Get("TicketDetailsFilePath", masterSheetRow);
                }
                else
                {
                    filePath = ConfigurationSettings.Get("FutureReportFilePath", masterSheetRow);
                }

                string fileName = Path.GetFileNameWithoutExtension(latestFile);
                int index = fileName.LastIndexOf('_');
                fileName = fileName.Remove(index);
                string date = DateTime.Now.ToShortDateString().ToString();
                fileName = fileName + "_" + date.Replace('/', '-');
                filePath = filePath + "\\" + fileName + ".xls";
                if (!File.Exists(filePath))
                {
                    app = new Application();
                    wb = app.Workbooks.Open(latestFile);
                    wb.SaveAs(filePath);
                    wb.Close();
                    app.Quit();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occurred as :->" + e.Message);
            }
        }
    }
}
