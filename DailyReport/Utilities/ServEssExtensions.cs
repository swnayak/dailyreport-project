﻿using CafeNextFramework.Helpers.Action;
using CafeNextFramework.Helpers.Verification;
using CafeNextFramework.TestAccess;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;


namespace DailyReport.Utilities
{
    public static class ServEssExtensions
    {

       public static bool VerifyElement(this IWebElement element, ISearchContext driver, string elementName, MasterSheetRow masterSheetRow) 
        {
            //Logger is not needed
            if (masterSheetRow != null && driver != null && element != null)
            {
                try
                {
                    //Verifying element is displayed.
                    if (driver.IsElementExists(masterSheetRow, element) && element.Displayed)
                    {
                        return true;
                    }
                }
                //Checking for element is displaying.If element is not displayed then it Will throw NoSuchElementException.
                catch (Exception e) when (e is NoSuchElementException)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return false;
        }


        public static void NavigateBack(this IWebDriver driver, string pageLink, string pageName, MasterSheetRow masterSheetRow)
        {
            if (driver != null && pageLink != null && masterSheetRow != null)
            {
                string currentPageUrl = driver.Url; //Get the Page Url after Click
                if (currentPageUrl != pageLink)
                {
                    driver.Navigate().Back();
                    masterSheetRow.TestResultLogger.Passed(pageName,
                                             "Current Page URL : " + currentPageUrl + "Previous Page URL : " + pageLink,
                                             "Action should be resulted in successful navigation",
                                             "Action is resulted in successful navigation");
                }
                else
                {
                    masterSheetRow.TestResultLogger.Failed(pageName,
                                             "Previous page Url : " + pageLink + " Current Page URL : " + currentPageUrl,
                                            "Should able to navigate back to " + pageName,
                                             "Not able to navigate back to " + pageName);
                }
            }
        }

        public static void PickValueFromDropdown(this IWebElement element, string valueToBeSelected, string selectBy,
             string elementName, MasterSheetRow masterSheetRow)
        {
            if (masterSheetRow != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(selectBy) &&
                        !string.IsNullOrEmpty(valueToBeSelected) && element.IsDisplayed("Drop down menu", "Drop down", masterSheetRow))
                    {
                        selectBy = selectBy.Trim().ToUpper(System.Globalization.CultureInfo.InvariantCulture);
                        SelectElement select = new SelectElement(element);
                        switch (selectBy)
                        {
                            case "VALUE":
                                select.SelectByValue(valueToBeSelected);
                                masterSheetRow.TestResultLogger.Passed("Select Value From DropDown", valueToBeSelected + " value is selected from " + elementName + " Dropdown",
                                   "Should able to select value from " + elementName + " DropDown ", "value is selected value " + elementName + " Dropdown");
                                break;

                            case "TEXT":
                                select.SelectByText(valueToBeSelected);
                                masterSheetRow.TestResultLogger.Passed("Select Value From DropDown", valueToBeSelected + " value is selected from " + elementName + " Dropdown",
                                   "Should able to select value from " + elementName + " DropDown ", "value is selected value " + elementName + " Dropdown");
                                break;

                            default:
                                masterSheetRow.TestResultLogger.Failed("Select Value From DropDown",
                                    "Invalid parameter type : " + selectBy + " given to select value from " + elementName + " Dropdown",
                                    "Should able to select value from " + elementName + " DropDown ", "Invalid parameter type given to select value from " + elementName + " Dropdown");
                                break;
                        }
                    }
                    else
                    {
                        masterSheetRow.TestResultLogger.Failed("Select Value From DropDown", "Unable to select value from " + elementName + " Dropdown",
                                    "Should able to select value from " + elementName + " DropDown ", "Unable to select value from " + elementName + " Dropdown");
                    }
                }
                catch (Exception e) when (e is NoSuchElementException || e is WebDriverException || e is ArgumentNullException || e is UnexpectedTagNameException)
                {
                    masterSheetRow.TestResultLogger.Failed(elementName, "Unexpected Exception occurred, Unable to select value from " + elementName + " Dropdown",
                              "Should able to select value from DropDown ", "Unable to select value from Dropdown " + e.Message);
                }
            }
        }
        // Navigate To Specific Frame
        public static bool NavigateToSpecificFrame(this IWebDriver driver, string frameId, MasterSheetRow masterSheetRow)
        {
            if (masterSheetRow != null)
            {
                try
                {
                    if (!string.IsNullOrEmpty(frameId) && driver != null)
                    {
                        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
                        wait.Until(ExpectedConditions.FrameToBeAvailableAndSwitchToIt(frameId));
                        masterSheetRow.TestResultLogger.Passed("Switch Frame", "Successfully Navigated to Frame, FrameID is: " + frameId,
                      "Should able to navigate to Frame", "Successfully Navigated to Frame");
                        return true;
                    }
                    else
                    {
                        masterSheetRow.TestResultLogger.Failed("Switch Frame", "Unable to navigate to Frame, FrameId is not Present or webDriver Object is null",
                       "Should able to navigate to Frame", "Unable to navigate to Frame");
                    }
                }
                catch (Exception e) when (e is NoSuchFrameException || e is WebDriverTimeoutException)
                {
                    masterSheetRow.TestResultLogger.Failed("Switch Frame", "NoSuchFrameException occurred, Unable to navigate to Frame",
                        "Should able to navigate to Frame", "NoSuchFrameException occurred, Unable to navigate to Frame " + e.Message);
                }
            }
            return false;
        }
    }
}