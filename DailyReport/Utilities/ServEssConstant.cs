﻿

namespace DailyReport.Utilities
{
    internal static class ServEssConstant
    {
        public const int FRAMEID = 1;
        public const string FilterTextbox = "Incident";
        public const string SERVESSFUTUREBREACHEPAGE = "ServEss Futurebreached page";
    }
}
