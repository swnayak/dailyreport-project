﻿using CafeNextFramework;
using System;

namespace DailyReport
{
    public static class Program
    {
        private static void Main()
        {
            Console.WriteLine("TestCase Execution being started!!!!");
            try
            {
                ScriptRunner scriptRunner = new ScriptRunner();
                scriptRunner.Run();
            }
            catch (CafeNextFrameworkException ex)
            {
                Console.WriteLine("Unexpected Exception Occurred : " + ex.Message);
            }
            Console.WriteLine("Test Execution being completed");
            Console.ReadKey();
        }
    }
}